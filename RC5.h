//
// Created by ivan on 06.10.21.
//

#ifndef INFORMATIONPROTECTION_RC5_H
#define INFORMATIONPROTECTION_RC5_H

#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <filesystem>
#include <cmath>
#include <cstring>
#include "IProtection.h"

namespace ip
{
    template<typename Word_T>
    class RC5 : public ip::IProtection
    {
    public:
        /* w - word size in bits, r - number of rounds, b - length of key in bytes, K - key itself*/
        RC5(uint w, uint r, uint b, const unsigned char *K);

        RC5(uint w, uint r, uint b, const char *K);

        ~RC5();

    private:
        void Init();

        inline Word_T rotate_left(Word_T x, uint32_t y);

        inline Word_T rotate_right(Word_T x, uint32_t y);

    public:
        std::pair<Word_T, Word_T> encrypt(const std::pair<Word_T, Word_T> &msg);

        std::pair<Word_T, Word_T> decrypt(const std::pair<Word_T, Word_T> &msg);

        // encrypts file with given path into [filename].enc format
        // returns path of a new file
        std::filesystem::path Encrypt(const std::filesystem::path &path) override;

        // decrypts file with given path into [filename].dec format
        // returns path of the new file
        std::filesystem::path Decrypt(const std::filesystem::path &path) override;

        /* returns encrypted msg*/
        std::string Encrypt(const std::string& msg) override;
        /* returns decrypted msg*/
        std::string Decrypt(const std::string& c_msg) override;
        /* returns size of the ciphered msg */
        std::size_t Encrypt(const unsigned char* msg, std::size_t size, unsigned char* c_msg) override;
        /* returns size of the enciphered msg */
        std::size_t Decrypt(const unsigned char* c_msg, std::size_t size, unsigned char* msg) override;

    private:
        uint32_t w; // length of words in bits (16, 32, 64)
        uint64_t u; // the length of word in bytes
        uint64_t b; // the length of key in bytes
        uint8_t *K; // the key, considered as an array of bytes
        uint64_t c; // the len of key in words
        Word_T *L; // a temporary working array used during key scheduling
        uint64_t r; // the number of rounds to use when encrypting data
        uint64_t t; // the number of round subkey required
        Word_T *S; // the round subkey words;
        Word_T Pw; // first magic constant
        Word_T Qw; // seconds magic constant
    };
}
template<typename Word_T>
ip::RC5<Word_T>::RC5(uint w, uint r, uint b, const unsigned char *K):
        w(w),
        r(r),
        b(b),
        u (w / 8),
        c(std::max(Word_T(1), (Word_T)ceil(8. * b/w))),
        t(2 * (r + 1))
{
    if(w == 16)
        this->Pw = 0xB7E1, this->Qw = 0x9E37;
    else if(w == 32)
        this->Pw = 0xB7E15163, this->Qw = 0x9E3779B9;
    else if(w == 64)
        this->Pw = 0xB7E151628AED2A6B, this->Qw = 0x9E3779B97F4A7C15;
    else
        throw std::runtime_error("w must be 16, 32 or 64. Other values are not allowed");

    this->K = new uint8_t[b+1];
    memcpy(this->K,K,b);

    this->L = new Word_T[c+1];
    memset(this->L,0,c+1);

    this->S = new Word_T[t + 1];
    memset(this->S, 0, t+1);

    Init();
}

template<typename Word_T>
void ip::RC5<Word_T>::Init()
{
    Word_T i, j, k, A, B;

    for (i = b-1, L[c-1] = 0; i != Word_T(-1); i--)
        L[i/u] = (L[i/u] << 8) + K[i];

    for (S[0] = Pw, i = 1; i < t; i++)
        S[i] = S[i-1] + Qw;

    for (A = B = i = j = k = 0; k < 3 * t; k++, i = (i+1) % t, j = (j+1) % c)
    {
        A = S[i] = rotate_left(S[i] + (A + B), 3);
        B = L[j] = rotate_left(L[j] + (A + B), (A + B));
    }
}

template<typename Word_T>
std::pair<Word_T, Word_T> ip::RC5<Word_T>::encrypt(const std::pair<Word_T, Word_T> &msg)
{
    Word_T i, A = msg.first + S[0], B = msg.second + S[1];

    for (i = 1; i <= r; i++)
    {
        A = rotate_left(A ^ B, B) + S[2*i];
        B = rotate_left(B ^ A, A) + S[2*i + 1];
    }
    return {A, B};
}

template<typename Word_T>
std::pair<Word_T, Word_T> ip::RC5<Word_T>::decrypt(const std::pair<Word_T, Word_T> &msg)
{
    Word_T i, B = msg.second, A = msg.first;

    for (i = r; i > 0; i--)
    {
        B = rotate_right(B - S[2*i + 1], A) ^ A;
        A = rotate_right(A - S[2*i], B) ^ B;
    }
    return {A - S[0], B - S[1]};
}


template<typename Word_T>
Word_T ip::RC5<Word_T>::rotate_left(Word_T x, uint32_t y)
{
    return (((x)<<(y&(w-1))) | ((x)>>(w-(y&(w-1)))));
}

template<typename Word_T>
Word_T ip::RC5<Word_T>::rotate_right(Word_T x, uint32_t y)
{
    return (((x)>>(y&(w-1))) | ((x)<<(w-(y&(w-1)))));
}

template<typename Word_T>
ip::RC5<Word_T>::RC5(uint w, uint r, uint b, const char *K) :
        RC5(w, r,b , (unsigned char*)(K))
{}

template<typename Word_T>
std::filesystem::path ip::RC5<Word_T>::Encrypt(const std::filesystem::path &path)
{
    if(!std::filesystem::exists(path))
        throw std::runtime_error("File " + path.string() + " does not exist!");

    std::filesystem::path enc_path(path.string() + ".enc");
    std::fstream msg(path, std::ios::in | std::ios::binary);
    std::fstream encrypted(enc_path, std::ios::out | std::ios::binary);
    if(!encrypted.is_open())
        throw std::runtime_error("File" + enc_path.string() + " was not opened!");
    if(!msg.is_open())
        throw std::runtime_error("File" + path.string() + " was not opened!");

    constexpr uint64_t buffer_size = 65536;

    Word_T buffer[buffer_size + 16]{ 0 };
    auto* buffer_uint8 = (uint8_t*)buffer;

    uint64_t readWords;

    for(std::streamsize readBytes;true;)
    {
        readBytes = msg.readsome((char*)&buffer, buffer_size * u);

        if(readBytes < buffer_size * (uint64_t)u)
        {
            uint8_t bytesToAdd = (readBytes % (u * 2) == 0 ? u * 2 : (u * 2 - readBytes % (u * 2)));
            for(auto i = readBytes; i < readBytes + bytesToAdd; ++i)
            {
                buffer_uint8[i] = bytesToAdd;
            }

            readBytes += bytesToAdd;
        }

        readWords = readBytes / u;
        for(uint64_t i = 0; i < readWords ; i += 2 )
        {
            auto text = encrypt(std::pair(buffer[i], buffer[i + 1]));
            buffer[i] = text.first, buffer[i + 1] = text.second;
        }
        encrypted.write((const char*)buffer, readBytes);
        if(readBytes < buffer_size * (uint64_t)u)
            break;
    }
    return enc_path;
}

template<typename Word_T>
ip::RC5<Word_T>::~RC5()
{
    delete[] this->K;
    delete[] this->L;
    delete[] this->S;
}

template<typename Word_T>
std::filesystem::path ip::RC5<Word_T>::Decrypt(const std::filesystem::path &path)
{
    if(!std::filesystem::exists(path))
        throw std::runtime_error("File " + path.string() + " does not exist!");
    std::filesystem::path dec_path(path.string() + ".dec");
    std::fstream msg(path, std::ios::in | std::ios::binary);
    std::fstream decrypted(dec_path, std::ios::out | std::ios::binary);
    if(!decrypted.is_open())
        throw std::runtime_error("File" + dec_path.string() + " was not opened!");
    if(!msg.is_open())
        throw std::runtime_error("File" + path.string() + " was not opened!");

    constexpr uint32_t buffer_size = 65536;

    Word_T buffer[buffer_size + 16]{ 0 };
    auto* buffer_uint8 = (uint8_t*)buffer;

    uint64_t readWords;

    for(std::streamsize readBytes;true;)
    {
        readBytes = msg.readsome((char*)&buffer, buffer_size * u);

        readWords = readBytes / u;
        for(int i = 0; i < readWords; i += 2 )
        {
            auto text = decrypt(std::pair(buffer[i], buffer[i + 1]));
            buffer[i] = text.first, buffer[i + 1] = text.second;
        }

        if(readBytes < buffer_size * u)
            readBytes -= buffer_uint8[readBytes - 1];

        decrypted.write((const char*)buffer, readBytes);

        if(readBytes < buffer_size * u)
            break;
    }
    return dec_path;
}

template<typename Word_T>
std::string ip::RC5<Word_T>::Encrypt(const std::string &msg)
{
    return std::string();
}

template<typename Word_T>
std::string ip::RC5<Word_T>::Decrypt(const std::string &c_msg)
{
    return std::string();
}

template<typename Word_T>
std::size_t ip::RC5<Word_T>::Encrypt(const unsigned char *msg, std::size_t size, unsigned char *c_msg)
{
    return 0;
}

template<typename Word_T>
std::size_t ip::RC5<Word_T>::Decrypt(const unsigned char *c_msg, std::size_t size, unsigned char *msg)
{
    return 0;
}


#endif //INFORMATIONPROTECTION_RC5_H
