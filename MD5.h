#ifndef BZF_MD5_H
#define BZF_MD5_H

#include <cstring>
#include <iostream>
#include <sstream>
#include <filesystem>
#include <fstream>
#define BUFFER_SIZE 1048576

class MD5
{
public:
    using size_type = uint32_t; // must be 32bit
    explicit MD5(const std::string &text);

    std::string Hex();
    unsigned char* UChar();
private:
    typedef unsigned char uint1; //  8bit
    void CalculateMD5(uint1 *msg, std::size_t size);

    void init();

    size_type state[4];

    // low level logic operations
    static inline uint32_t F(uint32_t x, uint32_t y, uint32_t z);

    static inline uint32_t G(uint32_t x, uint32_t y, uint32_t z);

    static inline uint32_t H(uint32_t x, uint32_t y, uint32_t z);

    static inline uint32_t I(uint32_t x, uint32_t y, uint32_t z);

    static inline uint32_t rotate_left(uint32_t x, uint32_t n);
};

void ReadFileIntoString(std::string& str, const std::filesystem::path& path);
#endif
