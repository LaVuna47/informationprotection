#include <memory>
#include <valarray>
#include "MD5.h"

template <typename T>
T swap_endian(T u)
{
    union
    {
        T u;
        unsigned char u8[sizeof(T)];
    } source, dest;

    source.u = u;

    for (size_t k = 0; k < sizeof(T); k++)
        dest.u8[k] = source.u8[sizeof(T) - k - 1];

    return dest.u;
}

inline uint32_t MD5::F(uint32_t x, uint32_t y, uint32_t z)
{
    return x & y | ~x & z;
}

inline uint32_t MD5::G(uint32_t x, uint32_t y, uint32_t z)
{
    return x & z | y & ~z;
}

inline uint32_t MD5::H(uint32_t x, uint32_t y, uint32_t z)
{
    return x ^ y ^ z;
}

inline uint32_t MD5::I(uint32_t x, uint32_t y, uint32_t z)
{
    return y ^ (x | ~z);
}

inline uint32_t MD5::rotate_left(uint32_t x, uint32_t n)
{
    return (x << n) | (x >> (32 - n));
}


MD5::MD5(const std::string &text)
{
    init();
    CalculateMD5((MD5::uint1 *) text.c_str(), text.length());
}


void MD5::init()
{
    state[0] = 0x67452301;
    state[1] = 0xefcdab89;
    state[2] = 0x98badcfe;
    state[3] = 0x10325476;
}


void MD5::CalculateMD5(MD5::uint1 *msg, std::size_t size)
{
    decltype(size) original_size = size;

    // зкопіювати msg у більший буфер X
    auto* X = new MD5::uint1[size + 100];
    memset(X,0,size + 100);
    memcpy(X, msg, size);

    // деяка ініціалізація
    uint32_t &A = state[0], &B = state[1], &C = state[2], &D = state[3];
    uint32_t AA = A, BB = B, CC = C, DD = D;
    uint32_t T[64] { 0 };

    // зробити доповнення
    X[size] = 0x80;
    if((size + 8) % 64 == 0)
        size += 8 + 64;
    else
        size += 8 + (64 - ((size + 8) % 64));

    uint64_t len = uint64_t (8) * original_size;
    memcpy(&X[size-8], (void*)(&len),8);

    // порахувати T
    for(int i = 0; i < 64; ++i)
    {
        T[i] = uint32_t ((double)4294967296 * std::abs(sin(i + 1)));
    }

    auto* XX = new uint32_t[size / 4];
    { // перетворення масиву uint1 X в uint4 X
        for(int j = 0, i = 0; i < size; i += 4, ++j)
        {
            uint32_t elem{0};
            memcpy((void*)&elem, &X[i], 4);
            XX[j] = elem;
        }
    }

    // все решта
    for (int i = 0;i < size / 4; i += 16)
    {
        AA = A, BB = B, CC = C, DD = D;
        // Round 1
        A = B + rotate_left((A+F(B, C, D)+XX[i+0]+T[0]), 7);
        D = A + rotate_left((D+F(A, B, C)+XX[i+1]+T[1]), 12);
        C = D + rotate_left((C+F(D, A, B)+XX[i+2]+T[2]), 17);
        B = C + rotate_left((B+F(C, D, A)+XX[i+3]+T[3]), 22);

        A = B + rotate_left((A+F(B, C, D)+XX[i+4]+T[4]), 7);
        D = A + rotate_left((D+F(A, B, C)+XX[i+5]+T[5]), 12);
        C = D + rotate_left((C+F(D, A, B)+XX[i+6]+T[6]), 17);
        B = C + rotate_left((B+F(C, D, A)+XX[i+7]+T[7]), 22);

        A = B + rotate_left((A+F(B, C, D)+XX[i+8]+T[8]), 7);
        D = A + rotate_left((D+F(A, B, C)+XX[i+9]+T[9]), 12);
        C = D + rotate_left((C+F(D, A, B)+XX[i+10]+T[10]), 17);
        B = C + rotate_left((B+F(C, D, A)+XX[i+11]+T[11]), 22);

        A = B + rotate_left((A+F(B, C, D)+XX[i+12]+T[12]), 7);
        D = A + rotate_left((D+F(A, B, C)+XX[i+13]+T[13]), 12);
        C = D + rotate_left((C+F(D, A, B)+XX[i+14]+T[14]), 17);
        B = C + rotate_left((B+F(C, D, A)+XX[i+15]+T[15]), 22);

        // Round 2
        A = B + rotate_left((A+G(B, C, D)+XX[i+1]+T[16]), 5);
        D = A + rotate_left((D+G(A, B, C)+XX[i+6]+T[17]), 9);
        C = D + rotate_left((C+G(D, A, B)+XX[i+11]+T[18]), 14);
        B = C + rotate_left((B+G(C, D, A)+XX[i+0]+T[19]), 20);

        A = B + rotate_left((A+G(B, C, D)+XX[i+5]+T[20]), 5);
        D = A + rotate_left((D+G(A, B, C)+XX[i+10]+T[21]), 9);
        C = D + rotate_left((C+G(D, A, B)+XX[i+15]+T[22]), 14);
        B = C + rotate_left((B+G(C, D, A)+XX[i+4]+T[23]), 20);

        A = B + rotate_left((A+G(B, C, D)+XX[i+9]+T[24]), 5);
        D = A + rotate_left((D+G(A, B, C)+XX[i+14]+T[25]), 9);
        C = D + rotate_left((C+G(D, A, B)+XX[i+3]+T[26]), 14);
        B = C + rotate_left((B+G(C, D, A)+XX[i+8]+T[27]), 20);

        A = B + rotate_left((A+G(B, C, D)+XX[i+13]+T[28]), 5);
        D = A + rotate_left((D+G(A, B, C)+XX[i+2]+T[29]), 9);
        C = D + rotate_left((C+G(D, A, B)+XX[i+7]+T[30]), 14);
        B = C + rotate_left((B+G(C, D, A)+XX[i+12]+T[31]), 20);

        // Round 3
        A = B + rotate_left((A+H(B, C, D)+XX[i+5]+T[32]), 4);
        D = A + rotate_left((D+H(A, B, C)+XX[i+8]+T[33]), 11);
        C = D + rotate_left((C+H(D, A, B)+XX[i+11]+T[34]), 16);
        B = C + rotate_left((B+H(C, D, A)+XX[i+14]+T[35]), 23);

        A = B + rotate_left((A+H(B, C, D)+XX[i+1]+T[36]), 4);
        D = A + rotate_left((D+H(A, B, C)+XX[i+4]+T[37]), 11);
        C = D + rotate_left((C+H(D, A, B)+XX[i+7]+T[38]), 16);
        B = C + rotate_left((B+H(C, D, A)+XX[i+10]+T[39]), 23);

        A = B + rotate_left((A+H(B, C, D)+XX[i+13]+T[40]), 4);
        D = A + rotate_left((D+H(A, B, C)+XX[i+0]+T[41]), 11);
        C = D + rotate_left((C+H(D, A, B)+XX[i+3]+T[42]), 16);
        B = C + rotate_left((B+H(C, D, A)+XX[i+6]+T[43]), 23);

        A = B + rotate_left((A+H(B, C, D)+XX[i+9]+T[44]), 4);
        D = A + rotate_left((D+H(A, B, C)+XX[i+12]+T[45]), 11);
        C = D + rotate_left((C+H(D, A, B)+XX[i+15]+T[46]), 16);
        B = C + rotate_left((B+H(C, D, A)+XX[i+2]+T[47]), 23);

        // Round 4
        A = B + rotate_left((A+I(B, C, D)+XX[i+0]+T[48]), 6);
        D = A + rotate_left((D+I(A, B, C)+XX[i+7]+T[49]), 10);
        C = D + rotate_left((C+I(D, A, B)+XX[i+14]+T[50]), 15);
        B = C + rotate_left((B+I(C, D, A)+XX[i+5]+T[51]), 21);

        A = B + rotate_left((A+I(B, C, D)+XX[i+12]+T[52]), 6);
        D = A + rotate_left((D+I(A, B, C)+XX[i+3]+T[53]), 10);
        C = D + rotate_left((C+I(D, A, B)+XX[i+10]+T[54]), 15);
        B = C + rotate_left((B+I(C, D, A)+XX[i+1]+T[55]), 21);

        A = B + rotate_left((A+I(B, C, D)+XX[i+8]+T[56]), 6);
        D = A + rotate_left((D+I(A, B, C)+XX[i+15]+T[57]), 10);
        C = D + rotate_left((C+I(D, A, B)+XX[i+6]+T[58]), 15);
        B = C + rotate_left((B+I(C, D, A)+XX[i+13]+T[59]), 21);

        A = B + rotate_left((A+I(B, C, D)+XX[i+4]+T[60]), 6);
        D = A + rotate_left((D+I(A, B, C)+XX[i+11]+T[61]), 10);
        C = D + rotate_left((C+I(D, A, B)+XX[i+2]+T[62]), 15);
        B = C + rotate_left((B+I(C, D, A)+XX[i+9]+T[63]), 21);

        A += AA;
        B += BB;
        C += CC;
        D += DD;
    }


    delete[] X;
    delete[] XX;
}

std::string MD5::Hex()
{
    std::stringstream s;

    std::string res;
    for(int i = 0 ; i < 4; ++i)
    {
        s.str("");
        s << std::hex << swap_endian(state[i]);
        std::string st = s.str();
        while(st.size() < 8) st = "0" + st;
        res += st;
    }
    return res;
}

unsigned char *MD5::UChar()
{
    auto* ch = new unsigned char[16];
    for(int i = 0; i < 4; ++i)
    {
        uint32_t st = swap_endian(state[i]);
        auto* st_u_ch = (unsigned char*)&st;
        for(int j = 0; j < 4; ++j) ch[i + j] = st_u_ch[j];
    }
    return ch;
}


void ReadFileIntoString(std::string& str, const std::filesystem::path& path)
{
    if(!std::filesystem::exists(path))
        throw std::runtime_error("file "  + path.string() + " does not exist!");

    std::fstream file(path,std::ios::in | std::ios::binary);
    if(!file.is_open())
        throw std::runtime_error("File " + path.string() + " could not be opened!");

    char buf[BUFFER_SIZE];
    while(true)
    {
        memset(buf, '\0', BUFFER_SIZE);
        std::streamsize bytesRead = file.readsome(buf, BUFFER_SIZE);
        str += std::string(buf, bytesRead);
        if(bytesRead < BUFFER_SIZE)
            break;
    }
}
