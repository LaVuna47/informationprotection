#include <iostream>
#include <vector>
#include <set>
#include <functional>
#include <filesystem>
#include <fstream>
#include <chrono>
#include <ctime>
#include <cstring>
#include <boost/program_options.hpp>
#include "RC5.h"
#include "MD5.h"
#define all(c) c.begin(), c.end()
using namespace std;
namespace po = boost::program_options;
typedef long long ll;
typedef long double ld;
const uint32_t w = 16;
const uint32_t r = 8;
const uint32_t b = 32;
using WORD = uint16_t;

uint8_t* ReadLastNBytes(const std::filesystem::path& path, std::streamsize n)
{
    std::fstream F(path, ios::in | ios::binary);
    F.seekg(n,ios_base::end);
    auto* buf = new uint8_t [n];
    F.read(reinterpret_cast<char*>(buf), n);

    return buf;
}
uint8_t* hex_decode(const char *in, size_t len, uint8_t *out)
{
    unsigned int i, t, hn, ln;
    for (t = 0,i = 0; i < len; i+=2,++t) {

        hn = in[i] > '9' ? in[i] - 'A' + 10 : in[i] - '0';
        ln = in[i+1] > '9' ? in[i+1] - 'A' + 10 : in[i+1] - '0';

        out[t] = (hn << 4 ) | ln;
    }

    return out;
}

std::string compute_256_hash(const std::string& password)
{
    std::string front = MD5(password).Hex();
    std::string bottom = MD5(front).Hex();
    return front + bottom;
}

int main(int argc, char ** argv)
{
    std::ios_base::sync_with_stdio(false);
    cin.tie(nullptr);


    try
    {
        po::options_description allOpts("encrypt file options");
        allOpts.add_options()
        ("help", "print help message")
        ("encrypt-file", po::value<std::string>(), "File that's going to be encrypted")
        ("decrypt-file", po::value<std::string>(), "File that's going to be decrypted")
        ("password,p", po::value<std::string>(), "key to file");


        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, allOpts), vm);
        po::notify(vm);

        if(vm.count("help"))
        {
            cout << allOpts << '\n';
            return 0;
        }
        if(vm.empty())
        {
            cout << "No options specified\n";
            cout << allOpts << '\n';
            return 0;
        }
        if(vm.size() == 3)
        {
            cout << "To many arguments. Only two must be specified at usage - file or message and password" << '\n';
            cout << allOpts << '\n';
            return 0;
        }
        if(!vm.count("password"))
        {
            cout << "Password must be specified!\n";
            cout << allOpts << '\n';
            return 0;
        }

        std::string password_hash_str = compute_256_hash(vm.at("password").as<std::string>());
        std::string password_hash2_str = compute_256_hash(password_hash_str);

        uint8_t password_hash[33] { 0 };
        uint8_t password_hash2[33] { 0 };

        hex_decode(password_hash_str.c_str(), password_hash_str.size(), password_hash);
        hex_decode(password_hash2_str.c_str(), password_hash2_str.size(), password_hash2);

        if(vm.count("encrypt-file"))
        {
            std::filesystem::path filePath(vm.at("encrypt-file").as<std::string>());
            ip::RC5<WORD> rc5(w, r, b, password_hash);
            utility::DoTimePoint();
            auto encFilePath = rc5.Encrypt(filePath);
            {
                std::fstream F(encFilePath, ios::app | ios::binary);
                F.write((char*)password_hash2, b);
            }
            cout << "Success. Encrypted file is " << encFilePath.string() << '\n';
            utility::LogTimePoint();
        }
        else if(vm.count("decrypt-file"))
        {
            std::filesystem::path filePath(vm.at("decrypt-file").as<std::string>());
            uint8_t real_hash[33] { 0 };
            { // read real hash from file
                ifstream F(filePath);
                if(F.is_open())
                {
                    F.seekg(std::filesystem::file_size(filePath) - b);
                    F.read((char*)real_hash, b);
                }
            }
            auto IsEqaul = [&]() -> bool
            {
                for(int i = 0; i < b; ++i)
                    if(real_hash[i] != password_hash2[i])
                        return false;
                return true;
            };
            if(!IsEqaul())
            {
                cout << "Failure. Either password is wrong or file is corrupted\n";
                return 0;
            }
            ip::RC5<WORD> rc5(w, r, b, password_hash);
            std::filesystem::resize_file(filePath, std::filesystem::file_size(filePath) - b);
            utility::DoTimePoint();
            auto decFileName = rc5.Decrypt(filePath);
            {
                std::fstream F(filePath, ios::app | ios::binary);
                F.write((char*)password_hash2, b);
            }
            cout << "Success. Decrypted file is " << decFileName << '\n';
            utility::LogTimePoint();
        }
    }
    catch(std::exception& e)
    {
        std::cerr << "Exception: " << e.what() << '\n';
    }
    catch(...)
    {
        std::cerr << "Exception: Unknown error\n";
    }
}
/*













 */