#include <iostream>
#include <vector>
#include <set>
#include <functional>
#include <filesystem>
#include <fstream>
#include <chrono>
#include <ctime>

#include "MD5.h"

#define all(c) c.begin(), c.end()

using namespace std;
typedef long long ll;
typedef long double ld;


int main(int argc, char ** argv)
{
    std::ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    try
    {
        std::string msg;
        if(argc == 1)
        {
            cout << "msg:" << std::flush;
            getline(cin, msg);
        }
        else
        {
            ReadFileIntoString(msg,argv[1]);
        }

        cout << "MD5 hash:" << MD5(msg).Hex() << '\n';
    }
    catch(std::exception& e)
    {
        std::cerr << "Exception: " << e.what() << '\n';
    }
    catch(...)
    {
        std::cerr << "Unknown error\n";
    }
}

/*
Tests:
aa: 4124bc0a9335c27f086f24ba207a4912
aaa: 47bce5c74f589f4867dbd57e9ca9f808

 */












