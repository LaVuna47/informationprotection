//
// Created by ivan on 09.11.21.
//

#ifndef INFORMATIONPROTECTION_DSA_H
#define INFORMATIONPROTECTION_DSA_H

#include "IProtection.h"
#include <openssl/bio.h>
#include <openssl/dsa.h>
#include <openssl/err.h>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <openssl/rand.h>

namespace ip
{
    class DSAProtection
    {
    public:
        // returns size of signature
        static uint32_t Sign(const unsigned char *msg, std::size_t msgSize,
                             const char *privateKeyFile, unsigned char* signature);
        // returns signature
        static std::string Sign(const std::string& msg, const std::filesystem::path& keyPath);
        static bool Verify(const unsigned char* msg, std::size_t msgSize, const unsigned char* signature, std::size_t sigSize, const char *publicKeyFile);
        static bool Verify(const std::string& msg, const std::string& signature, const std::filesystem::path& keyPath);
        static bool Verify(const std::string& msg, const std::filesystem::path& sigPath, const std::filesystem::path& keyPath);

        // Computes sha256 hash of a file and creates a signature from that hash. Returns path of a signature
        static std::filesystem::path Sign(const std::filesystem::path& msgPath, const std::filesystem::path& keyPath);
        // Checks correctness with sha256 hash of a file
        static bool Verify(const std::filesystem::path& msgPath, const std::filesystem::path& sigPath, const std::filesystem::path& keyPath);

        static void GenerateKeys();
    };

}
std::string CalcSHA256(const std::filesystem::path& path);
#endif //INFORMATIONPROTECTION_DSA_H
