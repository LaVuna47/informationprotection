//
// Created by ivan on 04.11.21.
//

#ifndef INFORMATIONPROTECTION_RSA_H
#define INFORMATIONPROTECTION_RSA_H
#include "IProtection.h"
#include <openssl/bio.h> /* BasicInput/Output streams */
#include <openssl/err.h> /* errors */
#include <openssl/ssl.h> /* core library */


namespace ip
{
    enum class PEM_TYPE { PUBLIC_KEY, PRIVATE_KEY};

    class RSAProtection : public ip::IProtection
    {
    public:
        RSAProtection(const std::filesystem::path& keyPath, PEM_TYPE keyType);
        RSAProtection();
        ~RSAProtection();
        std::string Encrypt(const std::string& msg) override;
        std::string Decrypt(const std::string& c_msg) override;
        std::size_t Encrypt(const unsigned char* msg, std::size_t size, unsigned char* c_msg) override;
        std::size_t Decrypt(const unsigned char* c_msg, std::size_t size, unsigned char* msg) override;

        std::filesystem::path Encrypt(const std::filesystem::path&) override;
        std::filesystem::path Decrypt(const std::filesystem::path&) override;

        static void GenerateKeys();


    private:
        RSA* m_Keypair = nullptr;
        BIGNUM* m_BigNum = nullptr; // stores public exponent
        BIO* m_PrivateKey = nullptr;
        BIO* m_PublicKey = nullptr;
        const uint32_t c_BlockSize = 210; // maximum size in bytes that can be encrypted at once
        const uint32_t c_E = RSA_F4; // public exponent
        const int32_t c_KeyLength = 2048;
    };
}

#endif //INFORMATIONPROTECTION_RSA_H
