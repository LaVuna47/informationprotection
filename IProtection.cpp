//
// Created by ivan on 04.11.21.
//

#include "IProtection.h"
#include <chrono>
#include <iostream>

decltype(std::chrono::system_clock::now()) timePoint;


void utility::DoTimePoint()
{
    timePoint = std::chrono::system_clock::now();
}

void utility::LogTimePoint()
{
    auto timePoint2 = std::chrono::system_clock::now();
    auto delta = timePoint2 - timePoint;
    std::cout << "Seconds: " << 0.001 * (double)std::chrono::duration_cast<std::chrono::milliseconds>(delta).count() << '\n';
}

void LogAndDoTimePoint()
{
    utility::LogTimePoint();
    utility::DoTimePoint();
}