//
// Created by ivan on 04.11.21.
//

#include "RSA.h"
#include <iostream>
#include <fstream>
#include <cstring>

ip::RSAProtection::RSAProtection()
{
    m_BigNum = BN_new();
    BN_set_word(m_BigNum, c_E);

    m_Keypair = RSA_new();
    RSA_generate_key_ex(m_Keypair, c_KeyLength, m_BigNum, nullptr);

    m_PrivateKey = BIO_new(BIO_s_mem());
    m_PublicKey = BIO_new(BIO_s_mem());

    PEM_write_bio_RSAPrivateKey(m_PrivateKey, m_Keypair, nullptr, nullptr, 0, nullptr, nullptr);
    PEM_write_bio_RSAPublicKey(m_PublicKey, m_Keypair);

}

ip::RSAProtection::RSAProtection(const std::filesystem::path &keyPath, ip::PEM_TYPE keyType)
{
    FILE* fd;
    fd = fopen(keyPath.c_str(),"r");
    unsigned char mKey[2048];
    memset(mKey,'\n',2048);
    fread(mKey, sizeof(unsigned char),2048, fd);
    if(keyType == ip::PEM_TYPE::PUBLIC_KEY)
    {
        m_PublicKey = BIO_new_mem_buf(mKey,(int)strlen((const char*)mKey));
        m_Keypair = PEM_read_bio_RSAPublicKey(m_PublicKey, nullptr, nullptr, nullptr);
    }
    else
    {
        m_PrivateKey = BIO_new_mem_buf(mKey,(int)strlen((const char*)mKey));
        m_Keypair = PEM_read_bio_RSAPrivateKey(m_PrivateKey, nullptr, nullptr, nullptr);
    }
    fclose(fd);
}

std::string ip::RSAProtection::Encrypt(const std::string &msg)
{
    if(msg.size() > c_BlockSize) // 214
        throw std::runtime_error("Message too long!");
    auto * c_msg = new unsigned char[BIO_pending(m_PublicKey)];
    std::size_t sizeBytes = Encrypt((unsigned char*)msg.c_str(), msg.size(), c_msg);
    std::string ret((char*)c_msg, sizeBytes);
    return ret;
}

std::string ip::RSAProtection::Decrypt(const std::string &c_msg)
{
    auto* msg = new unsigned char[BIO_pending(m_PrivateKey)];
    std::size_t sizeBytes = Decrypt((unsigned char*)c_msg.c_str(),c_msg.size(),msg);
    std::string ret((char*)msg, sizeBytes);
    return ret;
}

std::size_t ip::RSAProtection::Encrypt(const unsigned char *msg, std::size_t size, unsigned char *c_msg)
{
    if(size > c_BlockSize) // 214
        throw std::runtime_error("Message too long!");
    int res = RSA_public_encrypt((int)size, msg, c_msg, m_Keypair, RSA_PKCS1_OAEP_PADDING);
    if(res == -1)
        throw std::runtime_error("Error in RSA_public_encrypt function");
    return res;
}

std::size_t ip::RSAProtection::Decrypt(const unsigned char *c_msg, std::size_t size, unsigned char *msg)
{
    int res = RSA_private_decrypt((int)size, c_msg, msg, m_Keypair, RSA_PKCS1_OAEP_PADDING);
    if(res == -1)
        throw std::runtime_error("Error in RSA_private_decrypt function");
    return res;
}

std::filesystem::path ip::RSAProtection::Encrypt(const std::filesystem::path &path)
{
    if(!std::filesystem::exists(path))
        throw std::runtime_error("File " + path.string() + " does not exist!");

    std::filesystem::path enc_path(path.string() + ".enc");
    std::fstream msg(path, std::ios::in | std::ios::binary);
    std::fstream encrypted(enc_path, std::ios::out | std::ios::binary);
    if(!encrypted.is_open())
        throw std::runtime_error("File" + enc_path.string() + " was not opened!");
    if(!msg.is_open())
        throw std::runtime_error("File" + path.string() + " was not opened!");

    const uint64_t buffer_size =    1048740; // ~ 1 MB
    const uint64_t c_buffer_size =  1300420; // ~ 1.3 MB

    unsigned char buffer[buffer_size] { 0 };
    unsigned char c_msg[c_buffer_size] { 0 };
    for(std::streamsize readBytes;true;)
    {
        memset(buffer,0, buffer_size);
        memset(c_msg, 0, c_buffer_size);
        std::streamsize ciphered_size = 0;
        readBytes = msg.readsome((char*)buffer, buffer_size);
        uint64_t i;
        for(i = 0; i < readBytes / c_BlockSize; ++i)
        {
            Encrypt(buffer + i * c_BlockSize, c_BlockSize,c_msg + i*253);
            ciphered_size += 253;
        }
        if(readBytes % c_BlockSize != 0)
        {
            Encrypt(buffer + i * c_BlockSize, readBytes % c_BlockSize,c_msg + i*253);
            ciphered_size += 253;
        }
        encrypted.write((char*)c_msg, ciphered_size);

        if(readBytes < buffer_size)
        {
            break;
        }
    }
    return enc_path;
}

std::filesystem::path ip::RSAProtection::Decrypt(const std::filesystem::path &path)
{
    if(!std::filesystem::exists(path))
        throw std::runtime_error("File " + path.string() + " does not exist!");
    std::filesystem::path dec_path(path.string() + ".dec");
    std::fstream msg(path, std::ios::in | std::ios::binary);
    std::fstream decrypted(dec_path, std::ios::out | std::ios::binary);
    if(!decrypted.is_open())
        throw std::runtime_error("File" + dec_path.string() + " was not opened!");
    if(!msg.is_open())
        throw std::runtime_error("File" + path.string() + " was not opened!");

    const uint64_t buffer_size =   1300420; // ~ 1.3 MB
    const uint64_t c_buffer_size = 1048740; // ~ 1 MB

    unsigned char buffer[buffer_size] { 0 };
    unsigned char d_msg[c_buffer_size] { 0 };

    for(std::streamsize readBytes;true;)
    {
        memset(buffer,0, buffer_size);
        memset(d_msg, 0, c_buffer_size);
        std::streamsize deciphered_size = 0;
        readBytes = msg.readsome((char*)buffer,buffer_size);
        for(uint64_t i = 0, j = 0; i < readBytes; i += 253, j += c_BlockSize)
        {
            deciphered_size += (int)Decrypt(buffer + i,253,d_msg + j);
        }
        decrypted.write((char*)d_msg, deciphered_size);
        if(readBytes < buffer_size)
            break;
    }

    return dec_path;
}

ip::RSAProtection::~RSAProtection()
{
    RSA_free(m_Keypair);
    if(m_PrivateKey != nullptr)
        free(m_PrivateKey);
    if(m_PublicKey != nullptr)
        free(m_PublicKey);
    BN_free(m_BigNum);
}

void ip::RSAProtection::GenerateKeys()
{
    RSA* keypair = RSA_new();
    BIGNUM* bignum = BN_new();

    if(BN_set_word(bignum, RSA_F4) != 1)
        throw std::runtime_error("Error in BN_set_word function.");

    RSA_generate_key_ex(keypair, 2024, bignum, nullptr);

    // public key
    FILE* fp = fopen("public_rsa.key", "w");
    PEM_write_RSAPublicKey(fp, keypair);
    fclose(fp);

    // private key
    fp = fopen("private_rsa.key", "w");
    PEM_write_RSAPrivateKey(fp, keypair, nullptr, nullptr, 0, nullptr, nullptr);
    fclose(fp);

    RSA_free(keypair);
    free(bignum);
}




