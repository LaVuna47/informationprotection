#include <iostream>
#include <vector>
#include <iomanip>
#include <queue>
#include <algorithm>
#include <cmath>
#include <set>
#include <unordered_map>
#include <list>
#include <thread>
#include <memory>
#include <cstring>
#include <unordered_set>
#include "RSA.h"
#include <boost/program_options.hpp>

#define all(c) c.begin(), c.end()
using namespace std;
namespace po = boost::program_options;


int main(int argc, char** argv)
{
    std::ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    try
    {
        po::options_description allOpts("encrypt file options");
        allOpts.add_options()
                ("help", "print help message")
                ("key-gen", "Generates two file public_rsa.key, private_rsa.key with RSA keys")
                ("encrypt-file", po::value<std::string>(), "File that's going to be encrypted")
                ("decrypt-file", po::value<std::string>(), "File that's going to be decrypted")
                ("key-path,k", po::value<std::string>(), "path to file key");


        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, allOpts), vm);
        po::notify(vm);

        if(vm.count("help"))
        {
            cout << allOpts << '\n';
            return 0;
        }
        if(vm.empty())
        {
            cout << "No options specified\n";
            cout << allOpts << '\n';
            return 0;
        }
        if(vm.count("key-gen"))
        {
            ip::RSAProtection::GenerateKeys();
            std::cout << "Keys are successfully generated.\n";
            return 0;
        }
        if(!vm.count("key-path"))
        {
            std::cout << "Path to key should be specified!\n";
            cout << allOpts << '\n';
            return 0;
        }
        if(!vm.count("encrypt-file") && !vm.count("decrypt-file"))
        {
            std::cout << "File is not specified!\n";
            cout << allOpts << '\n';
            return 0;
        }

        std::filesystem::path keyPath(vm.at("key-path").as<std::string>());
        if(!std::filesystem::exists(keyPath))
            throw std::runtime_error("Path " + keyPath.string() + " does not exist!");
        std::filesystem::path path; // path to file that's going to be encrypted / decrypted

        // encrypt-file
        if(vm.count("encrypt-file"))
        {
            path = vm.at("encrypt-file").as<std::string>();
            if(!std::filesystem::exists(path))
                throw std::runtime_error("Path " + path.string() + " does not exist!");
            ip::RSAProtection rsa(keyPath,ip::PEM_TYPE::PUBLIC_KEY);
            utility::DoTimePoint();
            auto encryptedFile = rsa.Encrypt(path);
            std::cout << "Successful operation. Encrypted file: " << encryptedFile.string() << "\n";
            utility::LogTimePoint();
            return 0;
        }
        if(vm.count("decrypt-file"))
        {
            path = vm.at("decrypt-file").as<std::string>();
            if(!std::filesystem::exists(path))
                throw std::runtime_error("Path " + path.string() + " does not exist!");
            ip::RSAProtection rsa(keyPath, ip::PEM_TYPE::PRIVATE_KEY);
            utility::DoTimePoint();
            auto decryptedFile = rsa.Decrypt(path);
            std::cout << "Successful operation. Decrypted file: " << decryptedFile.string() << "\n";
            utility::LogTimePoint();
            return 0;
        }
    }
    catch(std::exception& e)
    {
        std::cerr << "Exception: " << e.what() << '\n';
    }
    catch(...)
    {
        std::cerr << "Exception: Unknown error\n";
    }
}























