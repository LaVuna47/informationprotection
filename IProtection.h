//
// Created by ivan on 04.11.21.
//

#ifndef INFORMATIONPROTECTION_IPROTECTION_H
#define INFORMATIONPROTECTION_IPROTECTION_H

#include <string>
#include <filesystem>

/* Information Protection */
namespace ip
{
    class IProtection
    {
    public:
        /* returns encrypted msg*/
        virtual std::string Encrypt(const std::string& msg) = 0;
        /* returns decrypted msg*/
        virtual std::string Decrypt(const std::string& c_msg) = 0;
        /* returns size of the ciphered msg */
        virtual std::size_t Encrypt(const unsigned char* msg, std::size_t size, unsigned char* c_msg) = 0;
        /* returns size of the enciphered msg */
        virtual std::size_t Decrypt(const unsigned char* c_msg, std::size_t size, unsigned char* msg) = 0;
        /* returns path to the encrypted file */
        virtual std::filesystem::path Encrypt(const std::filesystem::path& file) = 0;
        /* returns path to the decrypted file */
        virtual std::filesystem::path Decrypt(const std::filesystem::path& file) = 0;
    };
}

namespace utility
{
    void DoTimePoint();
    void LogTimePoint();
    void LogAndDoTimePoint();
}

#endif //INFORMATIONPROTECTION_IPROTECTION_H
