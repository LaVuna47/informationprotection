#include "DSA.h"
#include <boost/program_options.hpp>
#include "iostream"
#include "RSA.h"
#include "fstream"
using namespace std;
namespace po = boost::program_options;


int main(int argc, char* argv[])
{
    std::ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    try
    {
        po::options_description allOpts("encrypt file options");
        allOpts.add_options()
                ("help", "print help message")
                ("key-gen", "Generates two file public_dsa.key, private_dsa.key with DSA keys")
                ("sign-file", po::value<std::string>(), "File that is going to be signed")
                ("sign-msg", po::value<std::string>(), "Message that is going to be signed")
                ("key-path,k", po::value<std::string>(), "Path to the key: (private for decryption, public for encryption)")
                ("sign-path", po::value<std::string>(), "Path to the signature")
                ("verify", "Verifies that file is not being nor distorted nor auth-braked")
                ("file-path,file", po::value<std::string>(), "File to be verified")
                ("msg", po::value<std::string>(), "Message to be verified");

        po::variables_map vm;

        po::store(po::parse_command_line(argc, argv, allOpts), vm);
        if(vm.count("help"))
        {
            cout << allOpts << '\n';
            return 0;
        }
        if(vm.empty())
        {
            cout << "No options specified\n";
            cout << allOpts << '\n';
            return 0;
        }
        if(vm.count("key-gen"))
        {
            ip::DSAProtection::GenerateKeys();
            std::cout << "Keys are successfully generated.\n";
            return 0;
        }
        if(vm.count("sign-file"))
        {
            if(!vm.count("key-path"))
            {
                std::cout << "Key must be specified!\n";
                std::cout << allOpts << '\n';
                return 0;
            }
            std::filesystem::path msgPath(vm.at("sign-file").as<std::string>());
            std::filesystem::path keyPath(vm.at("key-path").as<std::string>());
            utility::DoTimePoint();
            auto signature = ip::DSAProtection::Sign(msgPath, keyPath);
            std::cout << "File " << vm.at("sign-file").as<std::string>() << " is successfully signed!\n";
            std::cout << "Signature generated: " << signature.string() << '\n';
            utility::LogTimePoint();
            return 0;
        }
        if(vm.count("sign-msg"))
        {
            if(!vm.count("key-path"))
            {
                std::cout << "Key must be specified!\n";
                std::cout << allOpts << '\n';
                return 0;
            }
            std::filesystem::path sigPath("signature.key");
            std::string msg(vm.at("sign-msg").as<std::string>());
            std::filesystem::path keyPath(vm.at("key-path").as<std::string>());
            utility::DoTimePoint();
            std::string signature = ip::DSAProtection::Sign(msg,vm.at("key-path").as<std::string>());
            {
                fstream fd(sigPath, std::ios::out | std::ios::binary | std::ios::trunc);
                fd.write(signature.c_str(), (int)signature.size());
            }
            std::cout << "Message is successfully signed!\n";
            std::cout << "Signature generated: " << sigPath << '\n';

            utility::LogTimePoint();
            return 0;
        }
        if(vm.count("verify"))
        {
            if(!vm.count("sign-path"))
            {
                std::cout << "Path to signature must be specified!\n";
                std::cout << allOpts << '\n';
                return 0;
            }
            if(!vm.count("key-path"))
            {
                std::cout << "Key must be specified!\n";
                std::cout << allOpts << '\n';
                return 0;
            }
            std::filesystem::path sigPath(vm.at("sign-path").as<std::string>());
            std::filesystem::path keyPath(vm.at("key-path").as<std::string>());

            if(vm.count("file-path"))
            {
                std::filesystem::path msgPath(vm.at("file-path").as<std::string>());
                utility::DoTimePoint();
                bool isValid = ip::DSAProtection::Verify(msgPath,sigPath,keyPath);
                if(isValid)
                {
                    std::cout << "\033[0;32m Verification succeeded. \033[0m\n";
                }
                else
                {
                    std::cout << "\033[0;31m Verification failed. \033[0m \n";
                }
                utility::LogTimePoint();
                return 0;
            }

            if(vm.count("msg"))
            {
                std::string  msg(vm.at("msg").as<std::string>());
                utility::DoTimePoint();
                bool isValid = ip::DSAProtection::Verify(msg, sigPath, keyPath);
                if(isValid)
                {
                    std::cout << "\033[0;32m Verification succeeded. \033[0m\n";
                }
                else
                {
                    std::cout << "\033[0;31m Verification failed. \033[0m \n";
                }
                utility::LogTimePoint();
                return 0;
            }
        }
        std::cout << "Failed. Try again!\n";
        std::cout << allOpts << '\n';

    }
    catch(std::exception& e)
    {
        std::cerr << "Exception: " << e.what() << '\n';
    }
    catch(...)
    {
        std::cerr << "Exception: Unknown error\n";
    }
}