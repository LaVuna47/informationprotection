#include <iostream>
#include <vector>
#include <set>
#include <functional>
#include <filesystem>
#include <fstream>
#include <chrono>
#include <ctime>
#define all(c) c.begin(), c.end()
using namespace std;
typedef long long ll;
typedef long double ld;


const string filename("sequence.txt");
const int64_t M = (1 << 22) - 1;
const int64_t A = 729;
const int64_t C = 233;
const int64_t X0 = 5;

// logs to file
#define _LOG(msg) \
{                 \
    stringstream ss; \
    ss << (msg); \
    auto end = std::chrono::system_clock::now(); \
    std::time_t end_time = std::chrono::system_clock::to_time_t(end); \
    fstream file(filename, std::ios::app | std::ios::in); \
    file << "[" << std::ctime(&end_time) << "]:" << ss.str() << '\n'; \
    file.close(); \
}

int64_t next(int64_t x)
{
    return (A * x + C) % M;
}


void config()
{
    // creating file if it does not exist
    if(!std::filesystem::exists(filename))
    {
        ofstream file(filename, std::ios::trunc);
        if (!file.is_open())
        {
            throw std::runtime_error("file was not opened");
        }
        file.close();
    }
}

void generateSequence(int n)
{
    int64_t x = X0;
    stringstream seq;
    for(int i = 0; i < n; ++i)
    {
        x = next(x);
        cout << x << ' ';
        seq << x << ' ';
    }
    cout << '\n';
    _LOG(seq.str());
}

uint64_t period(int64_t start)
{
    int64_t x = next(start);
    int64_t p = 1;
    while(x != start) ++p, x = next(x);
    return p;
}

int main(int argc, char ** argv)
{
    std::ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    try
    {
        config();

        int n;
        cout << "Enter quantity of numbers: " << std::flush;
        cin >> n;
        generateSequence(n);

        cout << "Period: " << period(X0) << '\n';
    }
    catch(std::exception& e)
    {
        std::cout << "Exception: " << e.what() << '\n';
    }
    catch(...)
    {
        std::cout << "Unknown error\n";
    }
}

/*
Tests:

 What is the period of normal generator?

 */










