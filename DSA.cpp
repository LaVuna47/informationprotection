//
// Created by ivan on 09.11.21.
//

#include "DSA.h"
#include <iostream>
#include <cstring>
#include "MD5.h"
//#define _CHECK_ERROR(rc, func_name) \
//{                               \
//    auto err = ERR_get_error();      \
//    if(rc != 1)                     \
//    {                   \
//        std::stringstream ss; \
//        ss << (func_name); \
//        ss << " failed, error: " << err  << ", " << std::hex << "0x" << err << '\n'; \
//        throw std::runtime_error(ss.str());                                          \
//    }                                   \
//}

std::string CalcSHA256(const std::filesystem::path& path)
{
    FILE *file = fopen(path.c_str(), "rb");
    if(!file)
        throw std::runtime_error("Could not open file: " + path.string());

    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    const int bufSize = 32768;
    unsigned char buffer[bufSize];
    std::size_t bytesRead = 0;
    while((bytesRead = fread(buffer, sizeof(u_char), bufSize, file)))
    {
        SHA256_Update(&sha256, buffer, bytesRead);
    }
    SHA256_Final(hash, &sha256);
    fclose(file);
    return {(char*)hash, SHA256_DIGEST_LENGTH};
}
void ip::DSAProtection::GenerateKeys()
{
    DSA* key = DSA_new();
    FILE* fd;

    unsigned char buffer[32];
    RAND_bytes(buffer, sizeof(buffer));
    DSA_generate_parameters_ex(key,2048, nullptr, 0, nullptr, nullptr, nullptr);
    DSA_generate_key(key);

    fd = fopen("public_dsa.key", "w");
    PEM_write_DSA_PUBKEY(fd,key);
    fclose(fd);

    fd = fopen("private_dsa.key", "w");
    PEM_write_DSAPrivateKey(fd,key, nullptr, nullptr,0, nullptr, nullptr);
    fclose(fd);

    DSA_free(key);
}

uint32_t ip::DSAProtection::Sign(const unsigned char *msg, std::size_t msgSize,
                                 const char *privateKeyFile, unsigned char* signature)
{
    char mKey[2048];
    memset(mKey,0,2048);
    FILE* fd;
    fd = fopen(privateKeyFile,"r");
    auto keySize = fread(mKey, sizeof(char),2048, fd);
    fclose(fd);

    BIO* privateKey_bio = BIO_new_mem_buf(mKey, (int)keySize);
    DSA* key = PEM_read_bio_DSAPrivateKey(privateKey_bio, nullptr, nullptr, nullptr);
    unsigned int sigLen = 0;
    DSA_sign(0, msg, (int)msgSize, signature, &sigLen, key);
    return sigLen;
}

bool ip::DSAProtection::Verify(const unsigned char *msg, std::size_t msgSize, const unsigned char *signature,
                               std::size_t sigSize, const char *publicKeyFile)
{
    char mKey[2048];
    memset(mKey,0,2048);
    FILE* fd = fopen(publicKeyFile,"r");
    auto mKeySize = fread(mKey, sizeof(char), 2048, fd);
    fclose(fd);

    BIO* publicKey_bio = BIO_new_mem_buf(mKey, (int)mKeySize);
    DSA* key = PEM_read_bio_DSA_PUBKEY(publicKey_bio, nullptr, nullptr, nullptr);

    return DSA_verify(0,msg,(int)msgSize,signature,(int)sigSize,key);
}

std::string ip::DSAProtection::Sign(const std::string& msg, const std::filesystem::path& keyPath)
{
    unsigned char signature[1024];
    auto sigLen = ip::DSAProtection::Sign((unsigned char*)msg.c_str(), msg.size(), keyPath.c_str(), signature);
    return {(char*)signature, sigLen};
}

bool
ip::DSAProtection::Verify(const std::string &msg, const std::string &signature, const std::filesystem::path& keyPath)
{
    return ip::DSAProtection::Verify((unsigned char*)msg.c_str(), msg.size(),(unsigned char*)signature.c_str(),signature.size(),keyPath.c_str());
}

std::filesystem::path
ip::DSAProtection::Sign(const std::filesystem::path &msgPath, const std::filesystem::path &keyPath)
{
    std::string sha256 = CalcSHA256(msgPath);
    std::string signature = Sign(sha256,keyPath);
    std::filesystem::path sigPath("signature.key");
    FILE* fd = fopen(sigPath.c_str(),"w");
    fwrite(signature.c_str(),1,signature.size(),fd);
    fclose(fd);
    return sigPath;
}

bool ip::DSAProtection::Verify(const std::filesystem::path &msgPath, const std::filesystem::path &sigPath,
                               const std::filesystem::path &keyPath)
{
    std::string sha256 = CalcSHA256(msgPath);

    std::fstream fd(sigPath, std::ios::in | std::ios::binary);
    if(!fd.is_open())
        throw std::runtime_error("Could not open " + sigPath.string());
    char buffer[32768];
    memset(buffer,0,32768);
    auto readBytes = fd.readsome(buffer,32768);
    std::string signature(buffer, readBytes);

    return Verify(sha256, signature, keyPath);
}

bool ip::DSAProtection::Verify(const std::string &msg, const std::filesystem::path &sigPath,
                               const std::filesystem::path &keyPath)
{
    std::fstream fd(sigPath, std::ios::in | std::ios::binary);
    if(!fd.is_open())
        throw std::runtime_error("Could not open " + sigPath.string());
    char buffer[32768];
    memset(buffer, 0, 32768);
    auto readBytes = fd.readsome(buffer, 32768);
    std::string signature(buffer, readBytes);
    return Verify(msg,signature,keyPath);
}


